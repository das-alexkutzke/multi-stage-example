Especialização em Desenvolvimento Ágil de Software

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

*Infra-estrutura para desenvolvimento e implantação de Software (DevOps)*

Prof. Alexander Robert Kutzke

# Exemplo multi-stage Dockerfile

Faça o clone e execute: 

```bash
docker build -t goapp:1.0 .
docker image ls | grep goapp
docker container run -ti goapp:1.0
git switch multi-stage
docker build -t goapp:1.0 .
docker image ls | grep goapp
```
